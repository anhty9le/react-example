import { Tabs, Tab, Col, Row, Container, Table } from 'react-bootstrap';
import React, { PureComponent } from "react";
import FormLogin from "./FormLogin";
import FormSignUp from "./FormSignUp";
import { ListFruite, FormSignUp2 } from "./FormSignUp";
import TableProfile from "./TableProfile";
import './NavTab.css';

class NavTab extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            key: 'home',
            fruites: ['Apple', 'Banana'],
            people: [
                { id: String(Math.random()).substr(2), firstName: 'a1', lastName: 'b1', email: 'aaa' },
                { id: String(Math.random()).substr(2), firstName: 'a2', lastName: 'b2', email: 'aaa' },
                { id: String(Math.random()).substr(2), firstName: 'a3', lastName: 'b3', email: 'aaa' },
                { id: String(Math.random()).substr(2), firstName: 'a4', lastName: 'b4', email: 'aaa' }
            ]

        };
    }

    // addFruite = (item) => {
    //     this.setState(prevState => {
    //         const nextState = [...prevState.fruites, item];

    //         return {
    //             fruites: nextState,
    //             inputValue: ''
    //         }
    //     });
    // }

    addPeople = (item) => {
        this.setState((prevState) => {
            const nextState = [...prevState.people, item];

            return {
                people: nextState,
            }
        });
    }

    removePeople = (id) => {
        let pos = this.state.people.findIndex((item) => item.id === id)
        console.log('index: ', id, ' ', pos);

        this.setState((prevState) => {
            prevState.people.splice(pos, 1);
            return { people: [...prevState.people] }
        });
        console.log('id: ' + id);
    }




    render() {
        return (
            <Tabs
                id="controlled-tab-example"
                activeKey={this.state.key}
                onSelect={key => this.setState({ key })}
            >
                <Tab eventKey="home" title="Home">
                    <Container fluid>
                        <Row>
                            <Col>
                                <FormLogin />
                            </Col>

                            <Col>

                                <FormSignUp2
                                    addPeople={this.addPeople}
                                />
                                {/* <FormSignUp
                                    onAddFruite={this.addFruite}
                                />
                                <ListFruite
                                    fruites={this.state.fruites}
                                    onAddFruite={this.addFruite}
                                /> */}


                            </Col>

                        </Row>
                    </Container>
                </Tab>
                <Tab eventKey="profile" title="Profile">
                    <TableProfile
                        listPeople={this.state.people}
                        removePeople={this.removePeople} />
                </Tab>
                <Tab eventKey="contact" title="Contact" disabled>
                </Tab>
            </Tabs>
        );
    }
}
export default NavTab;
