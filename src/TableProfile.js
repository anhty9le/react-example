import { Table } from 'react-bootstrap';
import React, { PureComponent } from "react";
import { FormSignUp2} from "./FormSignUp";

class TableProfile extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onClickClose = this.onClickClose.bind(this);
        
    }

    onClickClose(id) {
        this.props.removePeople(id);
      }
    

    render() {
        return (
            <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.listPeople.map((item, i) => (
                        <tr>
                            <td>{i+1}</td>
                            <td>{item.firstName}</td>
                            <td>{item.lastName}</td>
                            <td>{item.email}
                            <button type="button" className="close" 
                            onClick={() => this.onClickClose(item.id)}>&times;</button></td>
                           
                        </tr>)
                        )
                    }
                </tbody>
            </Table>
            
            </>
        )
    }
}
export default TableProfile;