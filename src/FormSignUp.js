import { Form, Button } from 'react-bootstrap';
import React from "react";



const ListItem = ({ value, onClick }) => (
  <li onClick={onClick}>{value}</li>
);

const List = ({ items, onItemClick }) => (
  <ul>
    {
      items.map((item, i) => <ListItem key={i} value={item} onClick={onItemClick} />)
    }
  </ul>
);

// class ListFruite extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       inputValue: '',
//     };

//   }

//   onClick = () => {
//     const { inputValue } = this.state;
//     if (inputValue) {
//       this.props.onAddFruite(inputValue)
//     }
//     this.setState({ inputValue: "" })
//   }

//   onChange = (e) => this.setState({ inputValue: e.target.value });

//   handleItemClick = (e) => { console.log(e.target.innerHTML) }

//   render() {
//     const { inputValue } = this.state;
//     const { fruites } = this.props;
//     return (
//       <div>
//         <input type="text" value={inputValue} onChange={this.onChange} />
//         <button onClick={this.onClick}>Add</button>
//         <List items={fruites} onItemClick={this.handleItemClick} />
//       </div>
//     );
//   }
// }



// class FormSignUp extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { name: '', message: '', email: '' };
//     this.handleChange = this.handleChange.bind(this);
//     this.handleSubmit = this.handleSubmit.bind(this);
//   }

//   handleChange(key) {
//     return function (e) {
//       var state = {};
//       state[key] = e.target.value;
//       this.setState(state);
//     }.bind(this);

//   }

//   handleSubmit(event) {
//     event.preventDefault();

//     var data = {
//       name: this.state.name,
//       email: this.state.email,
//       message: this.state.message
//     }
//     this.props.onAddFruite(data.name);
//     this.props.onAddFruite(data.email);

//     console.log('Hello ' + data.name + ', your message is: ' + data.message);

//   }

//   render() {
//     return (
//       <form onSubmit={this.handleSubmit}>
//         <label>
//           Name: <br />
//           <input type="text" value={this.state.name} onChange={this.handleChange('name')} />
//         </label>

//         <label>
//           Email: <br />
//           <input type="email" value={this.state.email} onChange={this.handleChange('email')} />
//         </label>

//         <label>
//           Message:<br />
//           <textarea rows="4" cols="50" type="text" value={this.state.message} onChange={this.handleChange('message')} />
//         </label>
//         <br />
//         <input type="submit" value="Sub" />
//         <br />
//       </form>
//     );
//   }
// }

class FormSignUp2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = { id:'',  firstName: '', lastName: '', email: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(key) {
    return function (e) {
      var state = {};
      state[key] = e.target.value;
      this.setState(state);
    }.bind(this);

  }

  handleSubmit(event) {
    event.preventDefault();

    let data = {
      id: String(Math.random()).substr(2),
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email
    }
    this.props.addPeople(data);
    this.setState({ firstName: "", lastName:"", email:""  })

    console.log('Hello ' + data.id + ' ' + data.lastName + ' ' + data.firstName + ', your message is: ' + data.message);

  }
  render() {
    return (
      <Form onSubmit={this.handleSubmit}> 

        <Form.Group controlId="formBasicFirstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control type="firstName" placeholder="Enter First Name" value={this.state.firstName} onChange={this.handleChange('firstName')} />
        </Form.Group>

        <Form.Group controlId="formBasicLastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control type="lastName" placeholder="Enter Last Name" value={this.state.lastName} onChange={this.handleChange('lastName')} />
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={this.state.email} onChange={this.handleChange('email')} />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" />
        </Form.Group>
        <Button variant="primary" type="submit" value="Submit">
          Submit
        </Button>
      </Form>
    )
  }
}



//export default FormSignUp;
export {  FormSignUp2 };
