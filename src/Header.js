import React, { PureComponent } from "react";
import "./Header.css";
import { Jumbotron, Container} from 'react-bootstrap';


class Header extends PureComponent {
    render() {
        return (
            <Jumbotron fluid>
            <Container>
              <h1>Fluid jumbotron</h1>
              <p>
                This is a modified jumbotron that occupies the entire horizontal space of
                its parent.
              </p>
            </Container>
          </Jumbotron>
        );
    }
}
export default Header;
